# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2011, 2016, 2019.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-20 01:39+0000\n"
"PO-Revision-Date: 2020-09-28 14:04+0200\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marek Laane"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "qiilaq69@gmail.com"

#: GeneralPage.cpp:241
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Tundub, et energiasäästuteenus ei tööta."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr ""

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr ""

#~ msgid ""
#~ "The activity service is running with bare functionalities.\n"
#~ "Names and icons of the activities might not be available."
#~ msgstr ""
#~ "Tegevuste teenus töötab kõige elementaarsemal tasemel.\n"
#~ "Tegevuste nimed ja ikoonid ei pruugi olla kättesaadavad."

#~ msgid ""
#~ "The activity service is not running.\n"
#~ "It is necessary to have the activity manager running to configure "
#~ "activity-specific power management behavior."
#~ msgstr ""
#~ "Tegevuste teenus ei tööta.\n"
#~ "Tegevuspõhise toitehalduse seadistamiseks peab tegevuste haldur kindlasti "
#~ "töötama."

#~ msgid "Never turn off the screen"
#~ msgstr "Ekraani ei seisata kunagi"

#~ msgid "Never shut down the computer or let it go to sleep"
#~ msgstr "Arvutit ei seisata kunagi ega lasta minna unne"

#~ msgid "Do not use special settings"
#~ msgstr "Eriseadistusi ei kasutata"

#~ msgid "Define a special behavior"
#~ msgstr "Erikäitumise määramine"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Uni"

#~ msgid "Hibernate"
#~ msgstr "Talveuni"

#~ msgid "Shut down"
#~ msgstr "Seiskamine"

#~ msgid "Always"
#~ msgstr "Alati"

#~ msgid "after"
#~ msgstr "pärast"

#~ msgid " min"
#~ msgstr " min"

#~ msgid "PC running on AC power"
#~ msgstr "Vooluvõrgust töötav arvuti"

#~ msgid "PC running on battery power"
#~ msgstr "Aku peal töötav arvuti"

#~ msgid "PC running on low battery"
#~ msgstr "Peaaegu tühja aku peal töötav arvuti"

#~ msgctxt "This is meant to be: Act like activity %1"
#~ msgid "Activity \"%1\""
#~ msgstr "Tegevus \"%1\""

#~ msgid "Act like"
#~ msgstr "Tegutsemise viis"

#, fuzzy
#~| msgid "Use separate settings (advanced users only)"
#~ msgid "Use separate settings"
#~ msgstr "Eraldi seadistuste kasutamine (ainult kogenud kasutajatele)"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Tundub, et energiasäästuteenus ei tööta.\n"
#~ "Seda probleemi saab lahendada teenust käivitades või seda ajastades "
#~ "\"Käivitamise ja väljalülitamise\" all."

#~ msgid "Suspend"
#~ msgstr "Passiivseisund"

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "Arvutit ei seisata kunagi ega viida uneseisundisse"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "Tegevuste toitehalduse seadistused"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr "KDE toitehalduse süsteemi seadistamine tegevuste kaupa"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c) 2010: Dario Freddi"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr ""
#~ "Selles moodulis saab toitehalduse seadistusi täppishäälestada igale "
#~ "tegevusele eraldi."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Hooldaja"
